import { AppModule } from './app.module'
import { argv } from 'yargs'
import { MigrationsService } from './modules/migrations'
import { NestFactory } from '@nestjs/core'

async function bootstrap() {
	const app = await NestFactory.create(AppModule)
	if (argv.seed) {
		const migrations: MigrationsService = await app.get(MigrationsService)
		await migrations.seed()
		process.exit(0)
	}
	await app.listen(3000)
}
bootstrap()
