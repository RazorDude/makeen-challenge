import { ConfigProviderService } from '../configProvider'
import { Module } from '@nestjs/common'
import { SequelizeModule } from '@nestjs/sequelize'
import {
	AccessControlPoint,
	AccessControlPointsService,
	Collection,
	CollectionsService,
	Group,
	GroupsService,
	Item,
	ItemsService,
	Role,
	RolesService,
	RoleType,
	RoleTypesService,
	User,
	UsersService
} from './entities'

@Module({
	imports: [
		SequelizeModule.forRootAsync({
			useFactory: (configProvider: ConfigProviderService) => configProvider.config.db,
			inject: [ConfigProviderService]
		}),
		SequelizeModule.forFeature([
			AccessControlPoint,
			Collection,
			Group,
			Item,
			Role,
			RoleType,
			User
		])
	],
	providers: [
		AccessControlPointsService,
		CollectionsService,
		GroupsService,
		ItemsService,
		RolesService,
		RoleTypesService,
		UsersService
	],
	exports: [
		AccessControlPointsService,
		CollectionsService,
		GroupsService,
		ItemsService,
		RolesService,
		RoleTypesService,
		UsersService
	]
})
export class DBModule {}
