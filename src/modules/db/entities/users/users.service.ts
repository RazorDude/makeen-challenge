import { BaseEntityService } from '../baseEntity.service'
import { Injectable } from '@nestjs/common'
import { InjectModel } from '@nestjs/sequelize'
import { User } from './users.model'

@Injectable()
export class UsersService extends BaseEntityService<User> {
	constructor(
		@InjectModel(User)
		model: typeof User
	) {
		super(model)
	}
}
