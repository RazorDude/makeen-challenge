import { AccessControlPoint } from '../accessControlPoints'
import { Column, HasMany, Model, Table } from 'sequelize-typescript'
import { Role } from '../roles'

@Table
export class User extends Model<User> {
	@Column
	email: string;

	currentAccessControlPoints?: {[accessControlPointId: number]: AccessControlPoint}
	currentRole?: Role


	@HasMany(() => Role)
	roles: Role[]
}
