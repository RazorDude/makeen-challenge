import { BaseEntityService } from '../baseEntity.service'
import { Injectable } from '@nestjs/common'
import { InjectModel } from '@nestjs/sequelize'
import { RoleType } from './roleTypes.model'

@Injectable()
export class RoleTypesService extends BaseEntityService<RoleType> {
	constructor(
		@InjectModel(RoleType)
		model: typeof RoleType
	) {
		super(model)
	}
}
