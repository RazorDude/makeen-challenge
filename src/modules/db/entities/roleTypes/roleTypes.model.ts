import { AccessControlPoint } from '../accessControlPoints'
import { BelongsToMany, Column, HasMany, Model, Table } from 'sequelize-typescript'
import { Role } from '../roles'

@Table
export class RoleType extends Model<Role> {
	@Column
	name: string

	@HasMany(() => Role, 'typeId')
	roles: Role[]
	
	@BelongsToMany(() => AccessControlPoint, 'RoleTypeAccessControlPoints', 'typeId', 'accessControlPointId')
	accessControlPoints: AccessControlPoint[]
}
