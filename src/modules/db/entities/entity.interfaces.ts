import { Transaction } from 'sequelize'

export interface IDeleteOptions {
	filters: {[fieldName: string]: any}
	transaction?: Transaction
}

export interface IReadListResults<T> {
	page: number
	perPage: number
	items: T[]
	more: boolean
}

export interface IReadListOptions extends IReadOptions {
	order?: string[][]
	page?: number
	perPage?: number
	readAll?: boolean
}

export interface IReadOptions {
	filters: {[fieldName: string]: any}
	transaction?: Transaction
}

export interface IUpdateOptions {
	filters: {[fieldName: string]: any}
	transaction?: Transaction
}
