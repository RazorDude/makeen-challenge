import { BaseEntityService } from '../baseEntity.service'
import { Injectable } from '@nestjs/common'
import { InjectModel } from '@nestjs/sequelize'
import { Role } from './roles.model'

@Injectable()
export class RolesService extends BaseEntityService<Role> {
	constructor(
		@InjectModel(Role)
		model: typeof Role
	) {
		super(model)
	}
}
