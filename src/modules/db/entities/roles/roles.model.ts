import { AllowNull, BelongsTo, Column, Default, ForeignKey, Model, Table } from 'sequelize-typescript'
import { Group } from '../groups'
import { RoleType } from '../roleTypes'
import { User } from '../users'

@Table
export class Role extends Model<Role> {
	@ForeignKey(() => Group)
	groupId: number

	@ForeignKey(() => RoleType)
	typeId: number

	@ForeignKey(() => User)
	userId: number

	@AllowNull(false)
	@Default(false)
	@Column
	isCurrent: boolean


	@BelongsTo(() => Group)
	group: Group

	@BelongsTo(() => RoleType)
	type: RoleType

	@BelongsTo(() => User)
	user: User
}
