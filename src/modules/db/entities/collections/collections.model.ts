import { BelongsTo, Column, ForeignKey, HasMany, Model, Table } from 'sequelize-typescript'
import { Group } from '../groups'
import { Item } from '../items'

@Table
export class Collection extends Model<Collection> {
	@ForeignKey(() => Group)
	groupId: number

	@Column
	name: string


	@BelongsTo(() => Group)
	group: Group

	@HasMany(() => Item)
	items: Item[]
}
