import { BaseEntityService } from '../baseEntity.service'
import { Collection } from './collections.model'
import { Injectable } from '@nestjs/common'
import { InjectModel } from '@nestjs/sequelize'

@Injectable()
export class CollectionsService extends BaseEntityService<Collection> {
	constructor(
		@InjectModel(Collection)
		model: typeof Collection
	) {
		super(model)
	}
}
