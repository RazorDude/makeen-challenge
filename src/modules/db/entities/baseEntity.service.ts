import { BulkCreateOptions, CreateOptions, FindOptions } from 'sequelize'
import { IDeleteOptions, IReadListResults, IReadListOptions, IReadOptions, IUpdateOptions } from './entity.interfaces'
import { Model } from 'sequelize-typescript'

type Constructor<T> = new (...args: any[]) => T
type ModelType<T extends Model<T>> = Constructor<T> & typeof Model

export class BaseEntityService<T extends Model<T>> {
	constructor(
		public model: ModelType<T>
	) {
	}

	async create(data: {[fieldName: string]: any}, options?: CreateOptions): Promise<T> {
		return await this.model.create(data, options)
	}

	async bulkCreate(data: {[fieldName: string]: any}[], options?: BulkCreateOptions): Promise<T[]> {
		return await this.model.bulkCreate(data, options)
	}

	async readOne(options: IReadOptions): Promise<T | null> {
		return await this.model.findOne({...options, where: options.filters})
	}

	async readList(options: IReadListOptions): Promise<IReadListResults<T>> {
		const {
				filters,
				page: optionsPage,
				perPage: optionsPerPage,
				readAll: optionsReadAll
			} = options,
			page = optionsPage ? parseInt(optionsPage as any, 10) : 1, // make sure it's truly number - it could come as string from GET requests
			perPage = optionsPage ? parseInt(optionsPerPage as any, 10) : 10,
			readAll = (optionsReadAll === true) || (optionsReadAll as any === 'true')
		let readListOptions: FindOptions = {where: filters},
			readListResults: IReadListResults<T> = {page: 1, perPage: 0, items: [], more: false}
		if (!readAll) {
			readListOptions.offset = (page - 1) * perPage
			readListOptions.limit = perPage + 1
			readListResults.page = page
			readListResults.perPage = perPage
		}
		let items = await this.model.findAll({
			where: filters
		})
		if (readAll) {
			readListResults.perPage = items.length
		}
		else if (items.length === readListOptions.limit) {
			items.pop()
			readListResults.more = true
		}
		readListResults.items = items
		return readListResults
	}

	async update(data: {[fieldName: string]: any}, options: IUpdateOptions): Promise<[number, T[]]> {
		return await this.model.update(data, {...options, where: options.filters})
	}

	async delete(options: IDeleteOptions): Promise<number> {
		return await this.model.destroy({...options, where: options.filters})
	}
}
