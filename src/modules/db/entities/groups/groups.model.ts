import { Column, HasMany, Model, Table } from 'sequelize-typescript'
import { Collection } from '../collections'
import { Role } from '../roles'

@Table
export class Group extends Model<Group> {
	@Column
	name: string


	@HasMany(() => Collection)
	collections: Collection[]

	@HasMany(() => Role)
	roles: Role[]
}
