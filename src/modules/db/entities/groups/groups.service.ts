import { BaseEntityService } from '../baseEntity.service'
import { Group } from './groups.model'
import { Injectable } from '@nestjs/common'
import { InjectModel } from '@nestjs/sequelize'

@Injectable()
export class GroupsService extends BaseEntityService<Group> {
	constructor(
		@InjectModel(Group)
		model: typeof Group
	) {
		super(model)
	}
}
