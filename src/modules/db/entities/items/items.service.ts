import { BaseEntityService } from '../baseEntity.service'
import { Injectable } from '@nestjs/common'
import { InjectModel } from '@nestjs/sequelize'
import { Item } from './items.model'

@Injectable()
export class ItemsService extends BaseEntityService<Item> {
	constructor(
		@InjectModel(Item)
		model: typeof Item
	) {
		super(model)
	}
}
