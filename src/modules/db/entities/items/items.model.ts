import { BelongsTo, Column, ForeignKey, Model, Table } from 'sequelize-typescript'
import { Collection } from '../collections'

@Table
export class Item extends Model<Item> {
	@ForeignKey(() => Collection)
	collectionId: number

	@Column
	name: string


	@BelongsTo(() => Collection)
	collection: Collection
}
