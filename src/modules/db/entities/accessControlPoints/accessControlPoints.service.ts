import { AccessControlPoint } from './accessControlPoints.model'
import { BaseEntityService } from '../baseEntity.service'
import { Injectable } from '@nestjs/common'
import { InjectModel } from '@nestjs/sequelize'

@Injectable()
export class AccessControlPointsService extends BaseEntityService<AccessControlPoint> {
	constructor(
		@InjectModel(AccessControlPoint)
		model: typeof AccessControlPoint
	) {
		super(model)
	}
}
