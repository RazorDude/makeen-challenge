import { BelongsToMany, Column, DataType, Model, Table } from 'sequelize-typescript'
import { RoleType } from '../roleTypes'

@Table
export class AccessControlPoint extends Model<AccessControlPoint> {
	@Column
	method: string

	@Column
	requestFieldName?: string

	@Column(DataType.JSONB)
	routes: string[]

	@Column
	userFieldName?: string


	@BelongsToMany(() => RoleType, 'RoleTypeAccessControlPoints', 'accessControlPointId', 'typeId')
	roleTypes: RoleType[]
}
