import { ConfigProviderService } from './configProvider.service'
import { Global, Module } from '@nestjs/common'

@Global()
@Module({
	providers: [ConfigProviderService],
	exports: [ConfigProviderService]
})
export class ConfigProviderModule{
}
