import { IAppConfig } from './configProvider.interfaces'
import { Injectable } from '@nestjs/common'

@Injectable()
export class ConfigProviderService {
	public config: IAppConfig

	constructor() {
		this.config = {
			db: {
				dialect: 'postgres',
				host: '127.0.0.1',
				port: 5432,
				username: 'postgres',
				password: 'postgres',
				database: 'makeen_challenge',
				autoLoadModels: true,
				// synchronize: true
			},
			jwtSecret: '1234'
		}
	}
}
