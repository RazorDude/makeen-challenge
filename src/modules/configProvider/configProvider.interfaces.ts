import { SequelizeModuleOptions } from '@nestjs/sequelize'

export interface IAppConfig {
	db: SequelizeModuleOptions
	jwtSecret: string
}
