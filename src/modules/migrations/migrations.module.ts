import { DBModule } from '../db'
import { MigrationsService } from './migrations.service'
import { Module } from '@nestjs/common'

@Module({
	imports: [
		DBModule
	],
	providers: [
		MigrationsService
	],
	exports: [
		MigrationsService
	]
})
export class MigrationsModule {
}
