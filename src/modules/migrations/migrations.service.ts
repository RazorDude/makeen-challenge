// import { ConfigProviderService } from '../configProvider'
import {
	AccessControlPointsService,
	CollectionsService,
	GroupsService,
	ItemsService,
	RolesService,
	RoleTypesService,
	UsersService
} from '../db/entities'
import { Inject, Injectable } from '@nestjs/common'

@Injectable()
export class MigrationsService {
	constructor(
		@Inject(AccessControlPointsService)
		private accessControlPoints: AccessControlPointsService,

		@Inject(CollectionsService)
		private collections: CollectionsService,

		// @Inject(ConfigProviderService)
		// private configProvider: ConfigProviderService

		@Inject(GroupsService)
		private groups: GroupsService,

		@Inject(ItemsService)
		private items: ItemsService,

		@Inject(RolesService)
		private roles: RolesService,

		@Inject(RoleTypesService)
		private roleTypes: RoleTypesService,

		@Inject(UsersService)
		private users: UsersService
	) {
	}

	async seed(): Promise<void> {
		const instance = this,
			sequelize = this.roles.model.sequelize!
		await sequelize.transaction(async function(transaction) {
			await sequelize.sync({force: true})
			await instance.accessControlPoints.model.bulkCreate(
				[
					// unrsestricted access - intended for general managers (roleTypeId 1)
					{id: 1, method: 'all', routes: '["/collections", "/collections/:id"]'},
					{id: 2, method: 'all', routes: '["/groups", "/groups/:id"]'},
					{id: 3, method: 'all', routes: '["/items", "/items/:id"]'},
					{id: 4, method: 'all', routes: '["/roles", "/roles/:id"]'},
					{id: 5, method: 'all', routes: '["/users", "/users/:id"]'},

					// group-based access - intented for group managers
					// collections
					{id: 6, method: 'post', routes: '["/collections"]', requestFieldName: 'req.body.groupId', userFieldName: 'currentRole.groupId'},
					{id: 7, method: 'get', routes: '["/collections", "/collections/:id"]', requestFieldName: 'req.query.filters.groupId', userFieldName: 'currentRole.groupId'},
					{id: 8, method: 'patch', routes: '["/collections"]', requestFieldName: 'req.body.filters.groupId', userFieldName: 'currentRole.groupId'},
					{id: 9, method: 'delete', routes: '["/collections"]', requestFieldName: 'req.body.filters.groupId', userFieldName: 'currentRole.groupId'},
					// items
					{id: 10, method: 'post', routes: '["/items"]', requestFieldName: 'req.body.groupId', userFieldName: 'currentRole.groupId'},
					{id: 11, method: 'get', routes: '["/items", "/items/:id"]', requestFieldName: 'req.query.filters.$collection.groupId$', userFieldName: 'currentRole.groupId'},
					{id: 12, method: 'patch', routes: '["/items"]', requestFieldName: 'req.body.filters.$collection.groupId$', userFieldName: 'currentRole.groupId'},
					{id: 13, method: 'delete', routes: '["/items"]', requestFieldName: 'req.body.filters.$collection.groupId$', userFieldName: 'currentRole.groupId'},
					// roles
					{id: 10, method: 'post', routes: '["/roles"]', requestFieldName: 'req.body.groupId', userFieldName: 'currentRole.groupId'},
					{id: 11, method: 'get', routes: '["/roles", "/roles/:id"]', requestFieldName: 'req.query.filters.groupId', userFieldName: 'currentRole.groupId'},
					{id: 12, method: 'patch', routes: '["/roles"]', requestFieldName: 'req.body.filters.groupId', userFieldName: 'currentRole.groupId'},
					{id: 13, method: 'delete', routes: '["/roles"]', requestFieldName: 'req.body.filters.groupId', userFieldName: 'currentRole.groupId'},
				],
				{transaction}
			)
			await instance.roleTypes.model.bulkCreate(
				[
					{id: 1, name: 'Global Manager'},
					{id: 2, name: 'Group Manager'},
					{id: 3, name: 'Regular'}
				],
				{transaction}
			)
			await instance.users.model.bulkCreate(
				[
					{id: 1, email: 'generalmanager@fake.fake'},
					{id: 2, email: 'groupmanager@fake.fake'},
					{id: 3, email: 'regularmanager@fake.fake'}
				],
				{transaction}
			)
			await instance.groups.model.bulkCreate(
				[
					{id: 1, name: 'Group 1'},
					{id: 2, name: 'Group 2'},
					{id: 3, name: 'Group 3'}
				],
				{transaction}
			)
			await instance.collections.model.bulkCreate(
				[
					{id: 1, groupId: 1, name: 'Collection 1'},
					{id: 2, groupId: 1, name: 'Collection 2'},
					{id: 3, groupId: 2, name: 'Collection 3'},
					{id: 4, groupId: 3, name: 'Collection 4'}
				],
				{transaction}
			)
			await instance.items.model.bulkCreate(
				[
					{id: 1, collectionId: 1, name: 'Item 1'},
					{id: 2, collectionId: 1, name: 'Item 2'},
					{id: 3, collectionId: 2, name: 'Item 3'},
					{id: 4, collectionId: 3, name: 'Item 4'}
				],
				{transaction}
			)
			// intentionally, there are no role assignments for group id 3, so it can be demonstrated that nobody can access it, if such a demo is needed
			await instance.roles.model.bulkCreate(
				[
					{typeId: 1, userId: 1, groupId: null, isCurrent: true},
					{typeId: 2, userId: 1, groupId: 2, isCurrent: false},
					{typeId: 2, userId: 2, groupId: 2, isCurrent: true},
					{typeId: 3, userId: 3, groupId: 2, isCurrent: true}
				],
				{transaction}
			)
			await sequelize.query(
				`insert into "RoleTypeAccessControlPoints" ("typeId", "accessControlPointId") values ` +
				`(1, 1), (1, 2), (1, 3), (1, 4), (1, 5)`
				,
				{transaction}
			)
		})
	}
}
