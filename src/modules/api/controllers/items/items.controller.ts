import { BaseController } from '../base.controller'
import { Controller, Inject } from '@nestjs/common'
import { Item, ItemsService } from '../../../db/entities'

@Controller('items')
export class ItemsController extends BaseController<Item> {
	constructor(
		@Inject(ItemsService)
		dbEntityService: ItemsService
	) {
		super(
			{},
			dbEntityService
		)
	}
}
