import { BaseEntityService } from '../../db/entities'
import { Delete, Get, HttpException, HttpStatus, Patch, Post, Req, Res } from '@nestjs/common'
import { getNested, setNested } from '@ramster/general-tools'
import { Model } from 'sequelize-typescript'
import { Request, Response } from 'express'
import { User } from '../../db/entities'

export class BaseController<T extends Model<T>> {
	constructor(
		protected accessControlPointIds: {[methodName: string]: number[]},
		protected dbEntityService: BaseEntityService<T>
	) {}

	filterAccess(req: Request, res: Response, accessControlPointIds?: number[]): void {
		if (!accessControlPointIds) {
			return
		}
		if (!res.locals || !res.locals.user) {
			throw new HttpException('Unauthorized', HttpStatus.UNAUTHORIZED)
		}
		const user: User = res.locals.user,
			{ currentAccessControlPoints } = user
		let hasAccess = false
		if (!currentAccessControlPoints) {
			throw new HttpException('Forbidden', HttpStatus.FORBIDDEN)
		}
		for (const i in accessControlPointIds) {
			const accessControlPointData = currentAccessControlPoints[accessControlPointIds[i]]
			if (!accessControlPointData) {
				continue
			}
			if (!accessControlPointData.userFieldName) {
				hasAccess = true
				break
			}
			const userFieldValue = getNested(user, accessControlPointData.userFieldName),
				requestFieldValue = getNested(req, accessControlPointData.requestFieldName!)
			if ((typeof userFieldValue === 'undefined') || (typeof requestFieldValue === 'undefined')) {
				throw new HttpException('Forbidden', HttpStatus.FORBIDDEN)
			}
			const requestValueIsArray = requestFieldValue instanceof Array,
				valuesToTest = requestValueIsArray ? requestFieldValue : [requestFieldValue],
				valuesToTestAgainst = userFieldValue instanceof Array ? userFieldValue : [userFieldValue]
			let allowedValues: any[] = []
			valuesToTest.forEach((valueToTest: any) => {
				const valueToTestVariants = [
					valueToTest, // the value as-is
					parseInt(valueToTest, 10), // the int equivalent of the value
					parseFloat(valueToTest), // the float equivalent of the value
					(valueToTest === true) || (valueToTest === 'true') || (valueToTest === false) || (valueToTest === 'false') // the boolean equivalent of the value
				]
				for (const j in valuesToTestAgainst) {
					const valueToTestAgainst = valuesToTestAgainst[j]
					let matchFound = false
					for (const k in valueToTestVariants) {
						const variant = valueToTestVariants[k]
						if (valueToTestAgainst === variant) {
							allowedValues.push(variant)
							matchFound = true
							break
						}
					}
					if (matchFound) {
						break
					}
				}
			})
			if (!allowedValues.length) {
				continue
			}
			if (requestValueIsArray) {
				setNested(req, accessControlPointData.requestFieldName!, allowedValues)
			}
			hasAccess = true
			break
		}
		if (!hasAccess) {
			throw new HttpException('Forbidden', HttpStatus.FORBIDDEN)
		}
	}

	@Post()
	async create(
		@Req() req: Request,
		@Res() res: Response
	): Promise<void> {
		this.filterAccess(req, res, this.accessControlPointIds.create)
		res.json(await this.dbEntityService.create(req as any))
	}

	@Get(':id')
	async readOne(
		@Req() req: Request,
		@Res() res: Response
	): Promise<void> {
		this.filterAccess(req, res, this.accessControlPointIds.readOne)
		res.json(await this.dbEntityService.readOne(req as any))
	}

	@Get()
	async readList(
		@Req() req: Request,
		@Res() res: Response
	): Promise<void> {
		this.filterAccess(req, res, this.accessControlPointIds.readList)
		res.json(await this.dbEntityService.readList(req as any))
	}

	@Patch()
	async update(
		@Req() req: Request,
		@Res() res: Response
	): Promise<void> {
		this.filterAccess(req, res, this.accessControlPointIds.update)
		res.json(await this.dbEntityService.update(req.body.data, {filters: req.body.filters}))
	}

	@Delete()
	async delete(
		@Req() req: Request,
		@Res() res: Response
	): Promise<void> {
		this.filterAccess(req, res, this.accessControlPointIds.delete)
		res.json(await this.dbEntityService.delete(req.body))
	}
}
