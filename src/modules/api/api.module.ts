import { AuthenticationMiddleware } from '../security/middlewares'
import { ItemsController } from './controllers'
import { DBModule } from '../db'
import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common'

@Module({
	imports: [
		DBModule
	],
	controllers: [
		ItemsController
	]
})
export class APIModule implements NestModule {
	configure(consumer: MiddlewareConsumer) {
		consumer
			.apply(AuthenticationMiddleware)
			.forRoutes('*')
	}
}
