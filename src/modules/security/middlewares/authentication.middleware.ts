import { AccessControlPoint, Role, UsersService } from '../../db/entities'
import { ConfigProviderService } from '../../configProvider'
import { HttpException, HttpStatus, Inject, Injectable, NestMiddleware } from '@nestjs/common'
import * as jwt from 'jsonwebtoken'
import { NextFunction, Request, Response } from 'express'

@Injectable()
export class AuthenticationMiddleware implements NestMiddleware {
	constructor(
		@Inject(ConfigProviderService)
		private configProvider: ConfigProviderService,
		@Inject(UsersService)
		private usersService: UsersService
	) {
	}

	use(req: Request, res: Response, next: NextFunction) {
		const instance = this
		;(async function() {
			let authToken = req.headers.authorization
			if (typeof authToken !== 'string') {
				throw new HttpException('Unauthorized', HttpStatus.UNAUTHORIZED)
			}
			authToken = authToken.replace(/^Bearer\s/, '') as string
			const tokenData: {id: number} = await new Promise((resolve, reject) => {
				jwt.verify(
					authToken!,
					instance.configProvider.config.jwtSecret,
					(err, decoded) => {
						if (err) {
							reject(err)
						}
						resolve(decoded as any)
					}
				)
			})
			let userData = await instance.usersService.readOne({filters: {id: tokenData.id}})
			if (!userData) {
				throw new HttpException('Unauthorized', HttpStatus.UNAUTHORIZED)
			}
			const {roles} = userData
			let currentAccessControlPoints: {[id: number]: AccessControlPoint} = {},
				currentRole: Role | null = null
			for (const i in roles) {
				if (roles[i].isCurrent) {
					currentRole = roles[i]
					break
				}
			}
			if (!currentRole) {
				throw new HttpException('Unauthorized', HttpStatus.UNAUTHORIZED)
			}
			userData.currentRole = currentRole
			currentRole.type.accessControlPoints.forEach((accessControlPoint) => {
				currentAccessControlPoints[accessControlPoint.id] = accessControlPoint
			})
			userData.currentAccessControlPoints = currentAccessControlPoints
			if (!res.locals) {
				res.locals = {}
			}
			res.locals.user = userData
			next()
		})().then(
			() => next(),
			(err) => {
				res.status(err.status).end()
			}
		)
	}
}
