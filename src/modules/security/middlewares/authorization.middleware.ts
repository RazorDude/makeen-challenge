// import { ConfigProviderService } from '../../configProvider'
// import { getNested, setNested } from '@ramster/general-tools'
// import { HttpException, HttpStatus, Inject, Injectable, NestMiddleware } from '@nestjs/common'
// import { NextFunction, Request, Response } from 'express'
// import { Role, UsersService, AccessControlPoint } from '../../db/entities'

// @Injectable()
// export class AuthenticationMiddleware implements NestMiddleware {
// 	constructor(
// 		@Inject(ConfigProviderService)
// 		private configProvider: ConfigProviderService,
// 		@Inject(UsersService)
// 		private usersService: UsersService
// 	) {
// 	}

// 	use(req: Request, res: Response, next: NextFunction) {
// 		const instance = this
// 		;(async function() {
// 			if (!accessControlPointIds) {
// 				return
// 			}
// 			if (!res.locals || !res.locals.user) {
// 				throw new HttpException('Unauthorized', HttpStatus.UNAUTHORIZED)
// 			}
// 			const user: User = res.locals.user,
// 				{ currentAccessControlPoints } = user
// 			let hasAccess = false
// 			if (!currentAccessControlPoints) {
// 				throw new HttpException('Forbidden', HttpStatus.FORBIDDEN)
// 			}
// 			for (const i in accessControlPointIds) {
// 				const accessControlPointData = currentAccessControlPoints[accessControlPointIds[i]]
// 				if (!accessControlPointData) {
// 					continue
// 				}
// 				if (!accessControlPointData.userFieldName) {
// 					hasAccess = true
// 					break
// 				}
// 				const userFieldValue = getNested(user, accessControlPointData.userFieldName),
// 					requestFieldValue = getNested(req, accessControlPointData.requestFieldName!)
// 				if ((typeof userFieldValue === 'undefined') || (typeof requestFieldValue === 'undefined')) {
// 					throw new HttpException('Forbidden', HttpStatus.FORBIDDEN)
// 				}
// 				const requestValueIsArray = requestFieldValue instanceof Array,
// 					valuesToTest = requestValueIsArray ? requestFieldValue : [requestFieldValue],
// 					valuesToTestAgainst = userFieldValue instanceof Array ? userFieldValue : [userFieldValue]
// 				let allowedValues: any[] = []
// 				valuesToTest.forEach((valueToTest: any) => {
// 					const valueToTestVariants = [
// 						valueToTest, // the value as-is
// 						parseInt(valueToTest, 10), // the int equivalent of the value
// 						parseFloat(valueToTest), // the float equivalent of the value
// 						(valueToTest === true) || (valueToTest === 'true') || (valueToTest === false) || (valueToTest === 'false') // the boolean equivalent of the value
// 					]
// 					for (const j in valuesToTestAgainst) {
// 						const valueToTestAgainst = valuesToTestAgainst[j]
// 						let matchFound = false
// 						for (const k in valueToTestVariants) {
// 							const variant = valueToTestVariants[k]
// 							if (valueToTestAgainst === variant) {
// 								allowedValues.push(variant)
// 								matchFound = true
// 								break
// 							}
// 						}
// 						if (matchFound) {
// 							break
// 						}
// 					}
// 				})
// 				if (!allowedValues.length) {
// 					continue
// 				}
// 				if (requestValueIsArray) {
// 					setNested(req, accessControlPointData.requestFieldName!, allowedValues)
// 				}
// 				hasAccess = true
// 				break
// 			}
// 			if (!hasAccess) {
// 				throw new HttpException('Forbidden', HttpStatus.FORBIDDEN)
// 			}
// 		})().then(
// 			() => next(),
// 			(err) => {
// 				res.status(err.status).end()
// 			}
// 		)
// 	}
// }
