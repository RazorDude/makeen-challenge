import { DBModule } from '../db'
import { Global, Module } from '@nestjs/common'
import { SecurityService } from './security.service'

@Global()
@Module({
	imports: [
		DBModule
	],
	providers: [
		SecurityService
	],
	exports: [
		SecurityService
	]
})
export class SecurityModule{
}
