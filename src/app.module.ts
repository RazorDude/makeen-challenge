import { APIModule } from './modules/api'
import { ConfigProviderModule } from './modules/configProvider'
import { DBModule } from './modules/db'
import { MigrationsModule } from './modules/migrations'
import { Module } from '@nestjs/common'
import { SecurityModule } from './modules/security'

@Module({
	imports: [
		APIModule,
		ConfigProviderModule,
		DBModule,
		MigrationsModule,
		SecurityModule
	],
	providers: []
})
export class AppModule {}
